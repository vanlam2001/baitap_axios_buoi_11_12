let data = [];

const BASE_URL = "https://63bea7f7f5cfc0949b5d4856.mockapi.io";

function fetchQLNDList(){
    axios({
        url: `${BASE_URL}/quanlyuser`,
        method: "GET",
    })

    .then(function(res){
        renderQLNDList(res.data)
        data.push(...res.data);
    })

    .catch(function (err) {
        console.log("🚀 ~ file: scripts.js:14 ~ fetchQLNDList ~ err", err)  
    });
}

fetchQLNDList();


function themUser(){
    var isValid = validation();
    console.log("🚀 ~ file: scripts.js:26 ~ themUser ~  isValid",  isValid)
    let data = layThongTinTuFrom();
    if(isValid){
      axios({
        url: `${BASE_URL}/quanlyuser`,
        method: "POST",
        data: data
    })
    
    
    .then(function(res){
        fetchQLNDList();
        document.querySelector("#myModal .close").click();

    })

    .catch(function (err) {
        console.log("🚀 ~ file: scripts.js:14 ~ fetchQLNDList ~ err", err)  
    });

    }

    else{
        return;
    }
    
    
}

document.getElementById("themNguoiDung").addEventListener("click",function(){
    var footerEle = document.querySelector(".modal-footer");
    footerEle.innerHTML = `
        <button onclick="themUser()" class="btn btn-success">Thêm người dùng</button>
    `;
});


function xoaUser(id){
    axios({
        url: `${BASE_URL}/quanlyuser/${id}`,
        method: "DELETE",
    })

    .then(function(res){
        fetchQLNDList();
    })

    .catch(function (err) {
        console.log("🚀 ~ file: scripts.js:58 ~ xoaUser ~ err", err)
    });
}

function suaUser(id){
    axios({
        url: `${BASE_URL}/quanlyuser/${id}`,
        method: "GET",
    })

    .then(function(res){
        console.log("🚀 ~ file: scripts.js:69 ~ .then ~ res", res)
        showThongTinLenForm(res.data)
        $('#myModal').modal('show');
        var show = document.querySelector(".modal-footer");
    show.innerHTML = `<button onclick="capnhatDSND('${res.data.id}')" class="btn btn-secondary">Update</button>`
    })
    .catch(function (err) {
        console.log("🚀 ~ file: scripts.js:74 ~ suaUser ~ er", er)
    });

}

function capnhatDSND(){
    let data = layThongTinTuFrom();
    var isValid =  validate();

    if(isValid){
         axios({
        url: `${BASE_URL}/quanlyuser/${data.id}`,
        method: "PUT",
        data: data,
    })

    .then(function(res){
        console.log("🚀 ~ file: scripts.js:90 ~ .then ~ res", res)
        fetchQLNDList();
        document.querySelector("#myModal. close").click();
    })

    .catch(function (err) {
        console.log("🚀 ~ file: scripts.js:95 ~ capnhatDSND ~ err", err)
    });
    }
    
    else{
        return;
    }
}

function validation(){
    var ND = layThongTinTuFrom();
    
    var isValid = true;
    
    if(!kiemTraRong(ND.TaiKhoan, 0, "tbTK")){
        isValid = false;
    } else if(!kiemTraTrungTaiKhoan(ND.TaiKhoan, data)){
        isValid = false;
    }
    console.log(isValid)
    if(!kiemTraRong(ND.MatKhau, 1,"tbMK")){
        isValid = false;
    } else if(!kiemTraMatKhau(ND.MatKhau)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.HoTen, 2,"tbHT")){
        isValid = false;
    } else if(!kiemTraTen(ND.HoTen)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.Email, 3,"tbMail")){
        isValid = false;
    } else if(!kiemTraEmail(ND.Email)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.HinhAnh, 4,"tbHinhAnh")){
        isValid = false;
    } 
    console.log(isValid)

    if(!kiemTraChon(ND.LoaiNguoiDung, "tbLND")){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraChon(ND.NgonNgu, "tbNN")){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.MoTa, 5,"tbMT")){
        isValid = false;
    }

    else if(!kiemTraPhanMoTa(ND.MoTa, "tbMT", 60)){
       return false;
    }
    console.log(isValid)
    if(!isValid){
        
        return;
    } else{
        
        return ND;
    }
}

function validate(){
    var ND = layThongTinTuFrom();
    
    var isValid = true;
    
    if(!kiemTraRong(ND.TaiKhoan, 0, "tbTK")){
        isValid = false;
    } else if(kiemTraTrung(ND.TaiKhoan, data)){
        isValid = false;
    }
    console.log(isValid)
    if(!kiemTraRong(ND.MatKhau, 1,"tbMK")){
        isValid = false;
    } else if(!kiemTraMatKhau(ND.MatKhau)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.HoTen, 2,"tbHT")){
        isValid = false;
    } else if(!kiemTraTen(ND.HoTen)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.Email, 3,"tbMail")){
        isValid = false;
    } else if(!kiemTraEmail(ND.Email)){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.HinhAnh, 4,"tbHinhAnh")){
        isValid = false;
    } 
    console.log(isValid)

    if(!kiemTraChon(ND.LoaiNguoiDung, "tbLND")){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraChon(ND.NgonNgu, "tbNN")){
        isValid = false;
    }
    console.log(isValid)

    if(!kiemTraRong(ND.MoTa, 5,"tbMT")){
        isValid = false;
    }

    else if(!kiemTraPhanMoTa(ND.MoTa, "tbMT", 60)){
       return false;
    }
    console.log(isValid)
    if(!isValid){
        
        return;
    } else{
        
        return ND;
    }
}
















