let error = [
  "Vui lòng nhập tài khoản",
  "Vui lòng nhập mật khẩu",
  "Vui lòng nhập Họ Tên",
  "Vui lòng nhập email",
  "Vui lòng dán hình ảnh",
  "Vui lòng nhập mô tả (Lưu ý: không được vượt quá 60 ký tự)"
];



function kiemTraTrungTaiKhoan(idNV, nhanVienArr){
  var id = nhanVienArr.findIndex(function (item) {
      return item.TaiKhoan == idNV;
    });
    var a = document.getElementById("tbTK");
    if (id != -1) {
      return (
        (a.innerHTML = "Tài khoản đã tồn tại"),
        (a.style.display = "block"),
        false
      );
    } else {
      return (a.innerHTML = ""), true;
    }
}

function kiemTraRong(el, message, idNotify) {
  if (el == "") {
    return (
      (document.getElementById(idNotify).innerHTML = error[message]),
      (document.getElementById(idNotify).style.display = `block`),
      false
    );
  } else {
    return (
      (document.getElementById(idNotify).innerHTML = ""),
      (document.getElementById(idNotify).style.display = "none"),
      true
    );
  }
}

function kiemTraTen(value) {
  const re =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;

  var isNumber = re.test(value);
  var a = document.getElementById("tbHT");
  if (isNumber) {
    return (a.innerHTML = ``), true;
  } else {
    return (
      (a.innerHTML = `Tên nhân viên phải là chữ`),
      (a.style.display = "block"),
      false
    );
  }
}

function kiemTraEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  var a = document.getElementById("tbMail");
  if (isEmail) {
    return (a.innerText = ``), true;
  } else {
    return (
      (a.innerText = ` Email phải đúng định dạng`),
      (a.style.display = "block"),
      false
    );
  }
}

function kiemTraMatKhau(value) {
  const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;

  var isPass = re.test(value);
  var a = document.getElementById("tbMK");
  if (isPass) {
    return (a.innerText = ``), true;
  } else {
    return (
      (a.innerText = `Mật Khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`),
      (a.style.display = "block"),
      false
    );
  }

}

function kiemTraChon(value, idErr) {
  var value = value.trim();
  var a = document.getElementById(idErr);
  if (value == "0") {
    return (a.innerHTML = `Phải chọn`), (a.style.display = "block"), false;
  } else {
    return (a.innerHTML = ``), true;
  }
}


function kiemTraPhanMoTa(str, idErr, max) {
  var length = str.length;
  console.log(length);

  var check = document.getElementById(idErr);
  if (length > max) {
    return (
      (check.innerText = `Không được vượt quá ${max} ký tự`),
      (document.getElementById("tbMT").style.display = "block"),
      false
    );
  }

  else {
    return (check.innerText = ``), true;
  }

}

function searchLocationNV(taiKhoan, arr) {
  var viTri = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].taiKhoan == taiKhoan) {
      viTri = i;
    }
  }
  return viTri;
}

function checkDuplicate(el, idNotify, arr) {
  var viTri = searchLocationNV(el, arr);
  if (viTri != -1) {
    return (
      (getEl(idNotify).innerHTML = `Tài khoản đã tồn tại`),
      (getEl(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getEl(idNotify).innerHTML = ""),
      (getEl(idNotify).style.display = "none"),
      true
    );
  }
}

function kiemTraTrung(){
  axios({
     url: "https://63bea7f7f5cfc0949b5d4856.mockapi.io/quanlyuser",
     method: "GET",
  })
  .then(function(res){
    var isValid = kiemTraTrungTaiKhoan(res.data);
    console.log("🚀 ~ file: validate.js:159 ~ .then ~ isValid", isValid)
    
  })
  .catch(function(err){
    console.log(err);
  })
}


