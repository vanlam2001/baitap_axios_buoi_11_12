function renderQLNDList(qlndARR){
    var contentHTML = "";
     
    qlndARR.forEach(function(DS){
      var contentTr = `<tr>
      <td>${DS.id}</td>
      <td>${DS.TaiKhoan}</td>
      <td>${DS.MatKhau}</td>
      <td>${DS.HoTen}</td>
      <td>${DS.Email}</td>
      <td>${DS.HinhAnh}</td>
      <td>${DS.LoaiNguoiDung}</td>
      <td>${DS.NgonNgu}</td>
      <td>${DS.MoTa}</td>
      </td>
      <td>
      <button onclick="xoaUser(${DS.id})" class="btn btn-danger" >Xóa</button> 
       
      </td>
      <td>
      <button onclick="suaUser(${DS.id})" class="btn btn-warning" >Sửa</button>
      </td>
      
       
         
      </tr>`;
    contentHTML += contentTr;
    });

    document.getElementById('tblDanhSachNguoiDung').innerHTML = contentHTML;
}

function layThongTinTuFrom(){
    var id = document.getElementById('id').value;
    var TaiKhoan = document.getElementById('TaiKhoan').value;
    var MatKhau = document.getElementById('MatKhau').value;
    var HoTen = document.getElementById('HoTen').value;
    var Email = document.getElementById('Email').value;
    var HinhAnh = document.getElementById('HinhAnh').value;
    var LoaiNguoiDung = document.getElementById('LoaiNguoiDung').value;
    var NgonNgu = document.getElementById('NgonNgu').value;
    var MoTa = document.getElementById('MoTa').value;
    return{
        id: id,
        TaiKhoan: TaiKhoan,
        MatKhau: MatKhau,
        HoTen: HoTen,
        Email: Email,
        HinhAnh: HinhAnh,
        LoaiNguoiDung: LoaiNguoiDung,
        NgonNgu: NgonNgu, 
        MoTa: MoTa,
    };
}

function laythongtin(){
    var iduser = document.getElementById('id').value;
    var account = document.getElementById('TaiKhoan').value;
    var password = document.getElementById('MatKhau').value;
    var name = document.getElementById('HoTen').value;
    var mail = document.getElementById('Email').value;
    var image = document.getElementById('HinhAnh').value;
    var user = document.getElementById('LoaiNguoiDung').value;
    var language = document.getElementById('NgonNgu').value;
    var mota = document.getElementById('MoTa').value;
    return{
        iduser,
        account,
        password,
        name,
        mail,
        image,
        user,
        language,
        mota,

    };
}


function showThongTinLenForm(DS){
    document.getElementById('id').value = DS.id;
    document.getElementById('TaiKhoan').value = DS.TaiKhoan;
    document.getElementById('MatKhau').value = DS.MatKhau;
    document.getElementById('HoTen').value = DS.HoTen;
    document.getElementById('Email').value = DS.Email;
    document.getElementById('HinhAnh').value = DS.HinhAnh;
    document.getElementById('LoaiNguoiDung').value = DS.LoaiNguoiDung;
    document.getElementById('NgonNgu').value = DS.NgonNgu;
    document.getElementById('MoTa').value = DS.MoTa;
}

